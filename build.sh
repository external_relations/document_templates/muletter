#!/bin/bash

set -e
shopt -s extglob
shopt -s nullglob

WORKPLACES=(arch ceitec ctt czs econ fi fsps fss iba ics is kariera \
  lang law med mu muzeu ped phil press rect sci skm teiresias ucb \
  uct)

# Clean up
rm -rf /tmp/overleaf overleaf dist

# Prepare the files
xetex muletter.ins
latexmk -pdf muletter.dtx
latexmk -c muletter.dtx
rm *.{bbl,bib,glo,gls,hd,run.xml}
(cd example
 xetex example.ins)
mkdir /tmp/overleaf

for WORKPLACE in ${WORKPLACES[@]}; do
  TMPDIR=/tmp/overleaf/$WORKPLACE
  mkdir -p $TMPDIR/muletter
  # Copy top-level example files
  cp -v example/{$WORKPLACE{,-color}.tex,signature.pdf} muletter.cls $TMPDIR
  # Copy remaining files
  tar cv {label,logo}/muletter-$WORKPLACE-{english,czech}{,-color}.pdf locale/*.def \
    muletter.{dtx,pdf,ins} LICENSE.tex README.md | tar xC $TMPDIR/muletter
  (cd $TMPDIR
   # Typeset top-level example files
   for DOCUMENT in $WORKPLACE*.tex; do
      latexmk -pdf $DOCUMENT &
   done
   wait
   latexmk -c $WORKPLACE*.tex
   # Prepare a zip archive
   zip -r ../$WORKPLACE *)
done

mv -v /tmp/overleaf dist
mv dist/*.zip public/
